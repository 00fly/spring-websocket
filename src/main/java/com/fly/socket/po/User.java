package com.fly.socket.po;

public class User
{
    private Long uid;
    
    public Long getUid()
    {
        return uid;
    }
    
    public void setUid(Long uid)
    {
        this.uid = uid;
    }
    
}
