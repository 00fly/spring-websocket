package com.fly.socket;

import java.io.IOException;

import org.apache.commons.lang3.SystemUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class WebSocketApplication implements CommandLineRunner
{
    
    @Value("${server.port}")
    String serverPort;
    
    public static void main(String[] args)
    {
        SpringApplication.run(WebSocketApplication.class, args);
    }
    
    @Override
    public void run(String... args)
        throws IOException
    {
        if (SystemUtils.IS_OS_WINDOWS)
        {
            Runtime.getRuntime().exec("cmd.exe /c start /min http://127.0.0.1:" + serverPort);
        }
    }
}
