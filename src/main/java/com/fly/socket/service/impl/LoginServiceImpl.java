package com.fly.socket.service.impl;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.fly.socket.service.LoginService;

@Service
public class LoginServiceImpl implements LoginService
{
    private Map<String, Long> userMap = new ConcurrentHashMap<>();
    
    @Override
    public String getpwdbyname(String name)
    {
        return "1234";
    }
    
    @Override
    public Long getUidbyname(String name)
    {
        if (StringUtils.isNotEmpty(name))
        {
            if (!userMap.containsKey(name))
            {
                userMap.put(name, RandomUtils.nextLong(1, 100000000));
            }
            return userMap.get(name);
        }
        return 0L;
    }
    
    @Override
    public String getnamebyid(long id)
    {
        for (String name : userMap.keySet())
        {
            if (id == userMap.get(name))
            {
                return name;
            }
        }
        return null;
    }
    
}
