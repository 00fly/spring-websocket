package com.fly.socket.websocket;

import javax.annotation.Resource;

import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

/**
 * WebScoket配置处理器
 */
@Configuration
@EnableWebSocket
public class WebSocketConfig extends SpringBootServletInitializer implements WebSocketConfigurer
{
    @Resource
    MyWebSocketHandler handler;
    
    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry)
    {
        registry.addHandler(handler, "/ws").setAllowedOrigins("*").addInterceptors(new HandShake());
        registry.addHandler(handler, "/ws/sockjs").setAllowedOrigins("*").addInterceptors(new HandShake()).withSockJS();
    }
}
